<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function deleteTaskRoute($id)
    {
        return route('tasks.destroy', $id);
    }
    /** @test */
    public function unauthenticate_user_can_not_see_button_delete()
    {
        $response = $this->get(route('tasks.index'));
        $response->assertDontSeeText('Delete');
    }

    /** @test */
    public function authenticate_user_can_see_button_delete()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get(route('tasks.index'));
        $response->assertSeeText('Delete');
    }

    /** @test */
    public function unauthenticate_user_can_not_delete_task()
    {
        $task_id = -1;
        $response = $this->delete($this->deleteTaskRoute($task_id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_delete_task_by_id(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create()->toArray();
        $response = $this->delete($this->deleteTaskRoute($task['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('tasks', $task);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test */
    public function authenticate_user_can_not_delete_task_if_it_is_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $task_id = -1;
        $response = $this->delete($this->deleteTaskRoute($task_id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
