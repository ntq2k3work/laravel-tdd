<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{

    public function getListTaskRoute()
    {
        return route('tasks.index');
    }

    public function updateTaskViewRoute($id)
    {
        return route('tasks.edit',$id);
    }

    public function updateTaskRoute($id)
    {
        return route('tasks.update',$id);
    }

    /** @test */
    public function unauthenticate_user_can_see_not_update_button(): void
    {
        $response = $this->get($this->getListTaskRoute());
        $response->assertDontSeeText('Update');
    }

    /** @test */
    public function authenticate_user_can_see_update_button(): void
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getListTaskRoute());
        $response->assertSeeText('Update');
    }

    /** @test */
    public function unauthenticate_user_can_not_access_update_form()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->updateTaskViewRoute($task->id));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function unauthenticate_user_can_not_update_task()
    {
        $task = Task::factory()->create();
        $response = $this->put($this->updateTaskRoute($task->id));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticate_user_can_view_update_form_when_click_update_button()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->from($this->getListTaskRoute())->get($this->updateTaskViewRoute($task->id));
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function authenticate_user_can_view_update_form_when_fill_valid_task_id()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->updateTaskViewRoute($task->id));
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function authenticate_user_can_update_task_by_id()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $updated =Task::factory()->make(['name' => 'HEHE'.$task->id])->toArray();
        $response = $this->put($this->updateTaskRoute($task->id),$updated);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListTaskRoute());
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_id_is_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $task_id = -1;
        $updated =Task::factory()->make()->toArray();
        $response = $this->put($this->updateTaskRoute($task_id),$updated);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_name_field_is_empty()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $updated =Task::factory()->make(['name' => null ])->toArray();
        $response = $this->put($this->updateTaskRoute($task->id),$updated);
        $response = $this->from($this->updateTaskViewRoute($task->id))->put($this->updateTaskRoute($task->id));
        $response->assertRedirect($this->updateTaskViewRoute($task->id));
        $response->assertSessionHasErrors(['name']);
    }
}
