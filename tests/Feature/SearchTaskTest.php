<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Support\Str;
class SearchTaskTest extends TestCase
{
    /** @test */
    public function user_can_search_task_by_name(): void
    {
        // $response = $this->get('/');
        $task = Task::factory()->create();
        $response = $this->get(route('tasks.search',$task->name));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText($task->name);
    }
    /** @test */
    public function user_can_search_task_when_using_upper_name(): void
    {
        // $response = $this->get('/');
        $task = Task::factory()->create();
        $response = $this->get(route('tasks.search',Str::upper($task->name)));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText($task->name);
    }

}
