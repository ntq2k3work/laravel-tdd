@extends('layouts.app')

@section('content')
<ul class="navbar-nav me-auto w-25 float-right">
    <form class="d-flex" id="search-form" action="{{ route('tasks.search') }}">
        {{-- @csrf --}}
        <input class="form-control me-2" type="search" name="keyword" placeholder="Search" aria-label="Search" value="{{ isset($keyword) ? $keyword : '' }}">
        <button class="btn btn-outline-success" id="search_btn" type="submit" >Search</button>
    </form>
</ul>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Content</th>
            @if (Auth::check())
                <th scope="col">Actions</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($tasks as $task)
        <tr>
            <th scope="row">{{ $task->id }}</th>
            <td>{{ $task->name }}</td>
            <td style="white-space: pre-wrap;">{{ $task->content }}</td>
            @if (Auth::check())
                <td>
                    <a href=" {{ route('tasks.edit',$task->id) }} " class="btn btn-primary btn-sm">Update</a>
                    <form id="form-delete" action="{{ route('tasks.destroy',$task->id) }}" method="POST" style="display: inline-block;" >
                        @csrf
                        @method('DELETE')
                        <button type="submit" id="delete-btn" class="btn btn-danger btn-sm" onclick="return confirm('Bạn chắc chắn muốn xoá ?')">Delete</button>
                    </form>
                </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
.<nav aria-label="Page navigation">
  <ul class="pagination justify-content-center">
        {{ $tasks->links() }}
  </ul>
</nav>
<div class="modal" id="modal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
@endsection
