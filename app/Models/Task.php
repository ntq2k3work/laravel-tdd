<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    protected $fillable = ['name','content'];

    public function searchByName($keyword)
    {
        return $this->where('name', 'like', "%$keyword%")->latest()->paginate(20);
    }
}
