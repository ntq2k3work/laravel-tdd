<?php

use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('',[TaskController::class,'index'])->name('tasks.index');
Route::prefix('tasks')->group(function(){
    Route::post('/store',[TaskController::class,'store'])->name('tasks.store')->middleware('auth');
    Route::get('/create',[TaskController::class,'create'])->name('tasks.create')->middleware('auth');
    Route::delete('/destroy/{id}',[TaskController::class,'destroy'])->name('tasks.destroy')->middleware('auth');
    Route::get('/edit/{id}',[TaskController::class,'edit'])->name('tasks.edit')->middleware('auth');
    Route::put('/update/{id}',[TaskController::class,'update'])->name('tasks.update')->middleware('auth');
    Route::get('/search',[TaskController::class,'search'])->name('tasks.search');
});
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

